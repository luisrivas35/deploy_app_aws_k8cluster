FROM ubuntu:22.04

# Actualiza el sistema y instala los paquetes necesarios
WORKDIR /app
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y curl wget unzip git python3 python3-pip

RUN git clone https://github.com/kubernetes-incubator/kubespray.git && \
    cd kubespray && \
    pip install -r requirements.txt

COPY . .
RUN chmod 400 clase_key.pem

RUN mkdir -p /opt/app
ENV PROJECT_HOME /opt/app
COPY target/spring-boot-mongo-1.0.jar $PROJECT_HOME/spring-boot-mongo.jar
WORKDIR $PROJECT_HOME

EXPOSE 8585
CMD ["java", "-jar", "./spring-boot-mongo.jar"]



